<?php
/**
 * A Chiper class to encrypt/decrypt Datas
 *
 * This Class is made to protect Datas for eg in Databases. DO NOT USE THIS TO ENCRYPT PASSWORDS. FOR PASSWORDS USE BCRYPT
 *
 * PHP version7
 *
 * @author Nergon <webmaster@nergon.pw>
 * @version 1.0
 * @copyright 2018 Nergon Inc.
 * @link https://nergon.pw
 */
 class Chiper
 {
   public $key;
   public $chiper = "AES-128-CBC";
   public $ivlen;
   public $iv;

   function __construct($KEY)
   {
     $this->key = $KEY;
     if (in_array($this->chiper, openssl_get_cipher_methods())) {
       $this->ivlen = openssl_cipher_iv_length($this->chiper);
       $this->iv = openssl_random_pseudo_bytes($this->ivlen);
     }
   }

   function encrypt($plain) {
     $texttoencryt = base64_encode($plain);
     $chipertext = openssl_encrypt($texttoencryt, $this->chiper, $this->key, $options=0, $this->iv);
     return $chipertext;
   }

   function decrypt($text) {
      $original = openssl_decrypt($text, $this->chiper, $this->key, $options=0, $this->iv);
     return base64_decode($original);
   }

 }
 ?>
